const express = require("express");
// TODO : tạo ra để giúp hiển thị những request của mình lên serve (HTTP logger)
const morgan = require("morgan");
//TODO : tạo ra để sử dụng template engines, hiểu nôm na là viết được html ,css trong ứng dụng node js
const handlebars = require("express-handlebars");
const path = require("path");
const app = express();
const port = 3000;

// ** HTTP logger
app.use(morgan("combined"));
// ** Template engines

app.engine(
  "html",
  handlebars({
    extname: ".html", // ? đặt lại phần đuôi mở rộng ,có thể thay bằng gì cũng được, ở đây đặt là html cho dễ nhìn :v
  })
);
app.set("view engine", "html");
app.set("views", path.join(__dirname, "resources/views"));
/* =========================================
  CÁCH VIẾT COMMENT , khi cài better comment
 ! lỗi cmnr
 TODO : this function is not finish
 ? Is this still working
 //* this is hightlight
 * * star space star :)
*/
// console.log("PATH 1: ", __dirname); // ? =>   D:\NodeJS\Demo_Blog\src
// console.log("PATH : ", path.join(__dirname, "resources/views")); // ? =>   D:\NodeJS\Demo_Blog\src\views, có nghĩa là join vào thư mục views
app.get("/", (rep, res) => {
  res.render("home");
});

app.get("/news", (req, res) => res.render("news"));

// ** Sử dụng static files, có nghĩa bây giờ nếu đánh địa chỉ http://localhost:3000/images/logoNodejs.jpg sẽ hiện ra ảnh
app.use(express.static(path.join(__dirname, "public")));

app.listen(port, () =>
  console.log(`Project listening at http://localhost:${port}`)
);
