### Tạo project với NodeJS

- `npm init`

### Cài đặt nodemon để chạy live serve

- `npm i nodemon --save-dev`
- debug ứng dụng bằng bằng cách bật Node devtool
### Cài đặt Morgan

- Mục đích là giúp thấy được những request mà mình gửi lên serve

### Template engines
- giúp viết những file html ở những nơi khác trong ứng dụng nodejs 
- đề xuất sử dụng `express handlebars`
- `npm i express-handlebars`